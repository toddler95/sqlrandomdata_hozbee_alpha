CREATE TABLE `CustomerGroup` (
  `id` mediumint(8) unsigned NOT NULL auto_increment,
  `group` mediumint,
  `group_code` TEXT default NULL,
  PRIMARY KEY (`id`)
) AUTO_INCREMENT=1;

INSERT INTO `CustomerGroup` (`group`,`group_code`) VALUES (1,"torquent"),(2,"a"),(3,"mi"),(4,"aptent"),(5,"Maecenas"),(6,"tellus"),(7,"feugiat"),(8,"diam."),(9,"natoque"),(10,"enim");
