CREATE TABLE `deliveryboy` (
  `id` mediumint(8) unsigned NOT NULL auto_increment,
  `deliveryboy` mediumint,
  `name` varchar(255) default NULL,
  PRIMARY KEY (`id`)
) AUTO_INCREMENT=1;

INSERT INTO `deliveryboy` (`deliveryboy`,`name`) VALUES (1,"Nicholson, Yuri B."),(2,"Hebert, Erich C."),(3,"Nash, Keegan V."),(4,"Garner, Mohammad L."),(5,"Francis, Ronan N."),(6,"Andrews, Serena P."),(7,"Henry, Phelan R."),(8,"Mills, Karyn J."),(9,"Romero, Angelica H."),(10,"Elliott, Amanda I.");
